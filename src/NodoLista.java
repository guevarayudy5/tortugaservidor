public class NodoLista {

    public int valor;
    public NodoLista siguiente;

    public NodoLista(int valor) {
        this.valor = valor;
        this.siguiente = null;
    }

    public class AlgoritmoLiebreTortuga {

        public static boolean tieneCiclo(NodoLista cabeza) {
            if (cabeza == null || cabeza.siguiente == null) {
                return false;
            }

            NodoLista tortuga = cabeza;
            NodoLista liebre = cabeza.siguiente;

            while (tortuga != liebre) {
                if (liebre == null || liebre.siguiente == null) {
                    return false;
                }
                tortuga = tortuga.siguiente;
                liebre = liebre.siguiente.siguiente;
            }

            return true;
        }
    }

    public class BusquedaRepetidos {

        public static int encontrarRep(int[] numeros) {
            int tortuga = numeros[0];
            int liebre = numeros[0];
            do {

                tortuga = numeros[tortuga];
                liebre = numeros[numeros[liebre]];

            } while (tortuga != liebre);
            int ptr1 = numeros[0];
            int ptr2 = tortuga;

            while (ptr1 != ptr2) {
                ptr1 = numeros[ptr1];
                ptr2 = numeros[ptr2];
            }
            return ptr1;
        }
    }

}
