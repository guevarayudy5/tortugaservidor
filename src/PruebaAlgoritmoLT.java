
public class PruebaAlgoritmoLT {

    public static void main(String[] args) {
        // Crear una lista con ciclo
        NodoLista nodo1 = new NodoLista(1);
        NodoLista nodo2 = new NodoLista(2);
        NodoLista nodo3 = new NodoLista(3);
        NodoLista nodo4 = new NodoLista(4);
        nodo1.siguiente = nodo2;
        nodo2.siguiente = nodo3;
        nodo3.siguiente = nodo4;
        nodo4.siguiente = nodo2; // Esto crea un ciclo

        System.out.println(NodoLista.AlgoritmoLiebreTortuga.tieneCiclo(nodo1)); // Debería imprimir true

        // Crear una lista sin ciclo
        NodoLista nodo5 = new NodoLista(5);
        NodoLista nodo6 = new NodoLista(6);
        NodoLista nodo7 = new NodoLista(7);
        nodo5.siguiente = nodo6;
        nodo6.siguiente = nodo7;

        System.out.println(NodoLista.AlgoritmoLiebreTortuga.tieneCiclo(nodo5)); // Debería imprimir false


        int[] numeros = {1, 2, 3, 4, 5, 6, 7, 3}; // El número 3 se repite

        int numeroRepetido = NodoLista.BusquedaRepetidos.encontrarRep(numeros);
        System.out.println("Número repetido: " + numeroRepetido); // Debería imprimir 3

    }

}
